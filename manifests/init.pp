# == Class: kickstart
#
# Full description of class kickstart here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { kickstart:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Fabian Schneider <fabian.schneider@lemonize.de>
#
# === Copyright
#
# Copyright 2015 Lemonize
#
class kickstart (
    # mandatory
    $maven_user = undef,
    $maven_password = undef,
    $scmConnection = undef,
    $scmDeveloperConnection = undef,
    $scmUrl = undef,

    # should
    $customerShortName = 'customer',

    # optional
    $artifactId = undef,
    $groupId = undef,
    $javaClassPrefix = undef,
    $package = undef,
    $moduleName = undef,
    $projectName = undef,

    $kickstartVersion = '1.0.0-SNAPSHOT',
    $magnoliaVersion = '5.3.2'
){
    $CustomerShortName = capitalize($customerShortName)

    if $artifactId == undef {
        $ArtifactId = $customerShortName
    } else {
        $ArtifactId = $artifactId
    }


    if $groupId == undef {
        $GroupId = "de.lemonize.${customerShortName}"
    } else {
        $GroupId = $groupId
    }

    if $javaClassPrefix == undef {
        $JavaClassPrefix = $CustomerShortName
    } else {
        $JavaClassPrefix = $javaClassPrefix
    }

    if $package == undef {
        $Package = "de.lemonize.${customerShortName}"
    } else {
        $Package = $package
    }

    if $moduleName == undef {
        $ModuleName = $customerShortName
    } else {
        $ModuleName = $moduleName
    }

    if $projectName == undef {
        $ProjectName = $CustomerShortName
    } else {
        $ProjectName = $projectName
    }    

    $mvnCommand = "mvn archetype:generate -B -DarchetypeGroupId=io.kickstart -DarchetypeArtifactId=kickstart-archetype-website -DarchetypeVersion=1.0-SNAPSHOT -DarchetypeRepository=http://maven.lemonize.de/kickstart.snapshots -DgroupId=${GroupId} -DartifactId=${ArtifactId} -DjavaClassPrefix=${JavaClassPrefix} -Dpackage=${Package} -DkickstartVersion=${kickstartVersion} -DmagnoliaVersion=${magnoliaVersion} -DmoduleName=${ModuleName} -DprojectName=${ProjectName} -DscmConnection=${scmConnection} -DscmDeveloperConnection=${scmDeveloperConnection} -DscmUrl=${scmUrl}"
    package{'maven':
        ensure => installed,
    }
    ->
    file {'.m2':
        path => '/home/vagrant/.m2',
        ensure => directory,
        owner => 'vagrant',
        group => 'vagrant',
    }
    ->
    file {'settings':
        path => '/home/vagrant/.m2/settings.xml',
        ensure => file,
        content => template("kickstart/settings.erb"),
        owner => 'vagrant',
        group => 'vagrant',
    }
    ->
    exec{'maven kickstart archetype':
        command => $mvnCommand,
        creates => '/vagrant/${artifactId}/pom.xml',
        cwd     => '/vagrant',
        path    => ["/bin", "/usr/bin", "/usr/sbin"],
        user => root,
    }
}
